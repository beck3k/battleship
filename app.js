var expressjs = require('express');
var express = expressjs();
var server = require('http').Server(express);
var sio = require('socket.io')(server);

var port = 8000;

var players = 0;
var state = {'started' : false, 'players' : players, '0ready' : false, '1ready' : false};

console.log("Node> Starting...");
server.listen(port);
console.log('Node> Express started on ' + port);

express.use(expressjs.static(__dirname + '/public'));

express.get('/', function(req, res){
    console.log('Express> Request');
    res.sendFile(__dirname + '/index.html');
});

var pos = {};

var sockets = {};

var readyPlayers = 0;

sio.on('connection', (socket) => {
    console.log('Socket.io> Connection'); 
    players++;
    state.players = players;
    var myPlayer = players;
    var socketKeys = Object.keys(sockets);
    sockets[socket] = socketKeys.length + 1;
    console.log(sockets);
    console.log('Players: ' + players);
    socket.on('disconnect', (msg) => {
        console.log('Socket.io> Disconnection');
        players--;
        state.players = players;
    });
    socket.on('state', (msg) => {
       if(msg == 'get'){
           if(players == 2){
               state['period'] = 'pre';
           }
           sio.sockets.emit('state', state);
       }
    });
    socket.on('init', (msg) => {
       socket.emit('init', {player: myPlayer}); 
    });
    socket.on('updatePos', (msg) => {
        pos[msg.player] = msg;
        console.log(pos);
//        var posKeys = Object.keys(pos);
//        posKeys.forEach((e) => {
////           console.log(pos[e].ships.length); 
//        });
    });
    socket.on('ready', (msg) => {
//        console.log(sockets[socket]);
        readyPlayers++;
        state.readyPlayers = readyPlayers;
        console.log(state);
        if(readyPlayers == 2){
            state.started = true;
            state.period = 'playng';
            state.turn = 0;
            sio.sockets.emit('state', state);
            sio.sockets.emit('turn', state.turn);
        }
    });
    socket.on('enemyCoord', (msg) =>{
//        console.log();
//        console.log();
        console.log(msg);
        var obj = pos[(msg.enemy ? 1 : 0).toString()];
        var thePos = {};
        var found = false;
        for(var i in obj.ships){
            console.log(obj.ships[i]);
            console.log(msg.testPos);
            if(obj.ships[i].x == msg.testPos.x && obj.ships[i].y == msg.testPos.y){
                console.log('Battleship> Sunk: yep');
                found = true;
                thePos = obj.ships[i];
            }else {
                console.log('Battleship> Sunk: nope');
            }
        }
        sio.sockets.emit('sunk', {player: msg.enemy, sunk: found, pos: thePos});
    });
});